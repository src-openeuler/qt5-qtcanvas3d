Name:             qt5-qtcanvas3d
Version:          5.12.5
Release:          2
Summary:          Qt5 module for Canvas3d framework
License:          LGPLv2 with exceptions or GPLv3 with exceptions
Url:              http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:          https://download.qt.io/archive/qt/%{majmin}/%{version}/submodules/qtcanvas3d-everywhere-src-%{version}.tar.xz

BuildRequires:    make
BuildRequires:    qt5-rpm-macros >= %{version} qt5-qtbase-devel >= %{version}
BuildRequires:    qt5-qtbase-static
BuildRequires:    qt5-qtbase-private-devel qt5-qtdeclarative-devel
%{?_qt5:Requires: %{_qt5} = %{_qt5_version}}

%description
Qt5 module for Canvas3d framework.

%package examples
Summary:  Programming example provided for t5-qtcanvas3d
Requires: %{name} = %{version}-%{release}

%description examples
This package is a programming example for t5-qtcanvas3d.

%prep
%autosetup -n qtcanvas3d-everywhere-src-%{version} -p1

%build
%{qmake_qt5}

%make_build

%install
make install INSTALL_ROOT=%{buildroot}

%files
%license LICENSE.*
%{_qt5_qmldir}/QtCanvas3D/

%if 0%{?_qt5_examplesdir:1}
%files examples
%{_qt5_examplesdir}/canvas3d/
%endif

%changelog
* Thu Nov 10 2022 yaoxin <yaoxin30@h-partners.com> - 5.12.5-2
- Change Source

* Wed Oct 13 2021 peijiankang <peijiankang@kylinos.cn> - 5.12.5-1
- update to upstream version 5.12.5

* Mon Sep 14 2020 liuweibo <liuweibo10@huawei.com> - 5.11.1-4
- Fix Source0

* Fri Nov 15 2019 duyeyu <duyeyu@huawei.com> - 5.11.1-3
- Package init
